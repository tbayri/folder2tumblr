import logging
import time
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from watchdog.events import PatternMatchingEventHandler

class Watcher:
    def __init__(self):
        self.observer = Observer()

    def start(self, folder, on_created=False, patterns=["*.jpg", "*.jpeg", "*.png", "*.bmp", "*.pdf"], recursive=False):
        self.observer.start()
        event_handler = PatternMatchingEventHandler(patterns, ignore_directories=True)
        if on_created is not False:
            event_handler.on_created = on_created
        self.observer.schedule(event_handler, folder, recursive)
        try:
            while True:
                time.sleep(1)
        except KeyboardInterrupt:
            self.observer.stop()
