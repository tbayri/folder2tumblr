# -*- coding: UTF-8 -*-
"""
Folder to tumblr

Usage:
  folder2tumblr.py [options] BLOGNAME FOLDER JSONFILE [POSTSTATE]
  folder2tumblr.py -h | --help

Arguments:
    BLOGNAME        Blogname
    FOLDER=DIR      Folder
    JSONFILE=FILE   json file name
    POSTSTATE       the post state 'queue' or 'published' [default: published]

Options:
    -w --watch      Watch folder
"""

import os
import yaml
import json
import time
from docopt import docopt
from collections import OrderedDict

from uploader import Uploader
from watcher import Watcher

def wait_for_write_finish( filename ):
    historicalSize = -1
    while (historicalSize != os.path.getsize(filename)):
      historicalSize = os.path.getsize(filename)
      time.sleep(1)
    return True

if __name__ == "__main__":
    args = docopt(__doc__)

    blogname = args['BLOGNAME']
    folder = args['FOLDER']
    jsonfile = args['JSONFILE']
    post_state = args['POSTSTATE']
    yaml_path='.tumblr'
    jsonfile_exists_and_populated = False

    if not os.path.exists(yaml_path):
        print('Credential files does not exist, please create one or run interactive_console.py')
        exit(1)
    else:
        yaml_file = open(yaml_path, "r")
        tokens = yaml.safe_load(yaml_file)
        yaml_file.close()

    try:
        with open(jsonfile, 'r') as f:
            try:
                data = json.load(f, object_pairs_hook=OrderedDict)
                if len(data):
                    jsonfile_exists_and_populated = True
            except ValueError as err:
                jsonfile_exists_and_populated = False
    except FileNotFoundError as err:
        print("\033[90mData file doesn't exist, creating one...\033[39m")
        with open(jsonfile, 'w+') as f:
            json.dump({}, f, indent=4, sort_keys=True)
            print("\033[93mCreated [" + jsonfile + "]\033[39m")

    uploader = False
    try:
        uploader = Uploader(
            tokens['consumer_key'],
            tokens['consumer_secret'],
            tokens['oauth_token'],
            tokens['oauth_token_secret'],
            blogname,
            folder,
            jsonfile
        )
    except ValueError as err:
        print('\033[91mTumblr api error, details:\033[39m')
        print('\033[90m' + str(err) + '\033[39m')
        exit()

    if args['--watch'] is True:
        if jsonfile_exists_and_populated is False:
            try:
                print("\n\033[92m→ Database is empty, starting initial mass upload...\033[39m\n")
                uploader.mass_upload()
                print("\n\033[92m:) Mass upload successful.\033[39m")
            except ValueError:
                print("\n\033[91m:( Mass upload failed.\033[39m")
                exit()
        print("\n\033[92m→ Watching [" + folder + "]\033[39m\n")
        watch = Watcher()

        def on_created(event):
            try:
                if wait_for_write_finish(event.src_path) is True:
                    uploader.upload(event.src_path)
                    print('\033[90mUploaded [' + event.src_path + ']\033[39m')
            except FileNotFoundError as err:
                print(str(err))
            except ValueError as err:
                print('\033[93mFailed to upload [' + event.src_path + ']\033[39m')
                print('\tCause: ' + str(err))

        watch.start(folder, on_created)
    else:
        try:
            print("\n\033[92m→ Starting mass upload...\033[39m\n")
            uploader.mass_upload()
            print("\n\033[92m:) Mass upload successful.\033[39m")
        except ValueError:
            print("\n\033[91m:( Mass upload failed.\033[39m")
