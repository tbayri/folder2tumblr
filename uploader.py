import glob
import json
import os
import pytumblr as pytumblr
import time
import platform

from collections import OrderedDict
from os.path import basename
from time import gmtime, strftime

def get_uploadable_files(folder, uploaded_photos_json_file, file_mask=('*.jpg','*.png','*.gif','*.jpeg')):
    fields = ["creation_time", "filename"]
    already_uploaded_images = OrderedDict()

    if os.path.exists(uploaded_photos_json_file):
        with open(uploaded_photos_json_file, 'r+') as f:
            try:
                already_uploaded_images = json.load(f, object_pairs_hook=OrderedDict)
            except ValueError:
                already_uploaded_images = {}

    file_list = []
    for files in file_mask:
        file_list.extend(glob.glob(os.path.join(folder, files)))
        file_list.sort(key=os.path.getctime)

    result = OrderedDict()
    for file in file_list:
        if basename(file) in already_uploaded_images:
            print("\033[90m[" + file + "] already in database, skipping...\033[39m")
        else:
            creation_time = time.localtime(os.path.getctime(file))
            if platform.system() == 'Darwin':
                creation_time = time.localtime(os.stat(file).st_birthtime)
            result.update({
                basename(file) : strftime("%Y-%m-%d %H:%M:%S", creation_time)
            })
    return result

class Uploader:
    def __init__(self, consumer_key, consumer_secret, oauth_token, oauth_token_secret, blogname, folder, uploaded_images_json, file_mask=('*.jpg','*.png','*.gif','*.jpeg')):
        self.client = pytumblr.TumblrRestClient(
            consumer_key,
            consumer_secret,
            oauth_token,
            oauth_token_secret
        )

        info = self.client.info()
        if "meta" in info:
            raise ValueError(json.dumps(info['errors'], indent=4))

        self.blogname = blogname
        self.folder = folder
        self.uploaded_images_json = uploaded_images_json
        self.file_mask = file_mask

    def upload(self, image_file, timestamp=False, post_state="published"):
        if timestamp is False:
            if platform.system() == 'Darwin':
                creation = strftime('%Y-%m-%d %H:%M:%S', time.localtime(os.stat(image_file).st_birthtime))
            else:
                timestamp = strftime('%Y-%m-%d %H:%M:%S', time.localtime(os.path.getctime(image_file)))
        response = self.client.create_photo(
            self.blogname,
            state=post_state,
            data=image_file,
            caption=basename(image_file),
            date=timestamp
        )
        if ('meta' in response):
            raise ValueError('Error posting image [' + basename(image_file) + '], info:\n' + json.dumps(response, indent=4, sort_keys=True))
        else:
            try:
                self.update_json({
                    basename(image_file): strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
                })
                return response
            except ValueError as err:
                raise err;

    def mass_upload(self, post_state='published'):
        files = get_uploadable_files(self.folder, self.uploaded_images_json)

        uploadedImages = OrderedDict()
        for image, timestamp in files.items():
            try:
                response = self.upload(self.folder + os.path.sep + image, timestamp)
                uploadedImages.update({
                    image: strftime("%Y-%m-%d %H:%M:%S", time.gmtime())
                })
                if ("id" in response):
                    print("[" + image + "] posted as " + post_state + ", with the id: [" + str(response['id']) + "]")
                else:
                    print("[" + image + "] posted as " + post_state)
            except ValueError as error:
                print("Couldn't post file [" + image + "]")
        if len(uploadedImages) > 0:
            try:
                self.update_json(uploadedImages)
                print("Done uploading. Check [" + self.uploaded_images_json + "] for the complete list of uploaded material.")
            except ValueError as error:
                raise error

    def update_json(self, data):
        data_to_write = OrderedDict()
        try:
            with open(self.uploaded_images_json, 'r+') as f:
                try:
                    data_to_write = json.load(f, object_pairs_hook=OrderedDict)
                except ValueError:
                    print("\033[93mData file is empty or is unreadable\033[39m")
        except FileNotFoundError as err:
            raise err

        try:
            if len(data_to_write):
                with open(self.uploaded_images_json, 'w+') as f:
                    try:
                        data_to_write.update(data)
                        json.dump(data_to_write, f, indent = 4, sort_keys=True)
                    except ValueError as error:
                        raise error
            else:
                try:
                    with open(self.uploaded_images_json, 'w+') as f:
                        json.dump(data, f, indent = 4, sort_keys=True)
                except ValueError as error:
                    raise error
        except FileNotFoundError as err:
            raise err
